# This not officially released Minetest mod was moved

The repository was moved away from GitLab. The new location can be found here:

* https://git.0x7be.net/dirk/playtime-limit

Just set the new origin in your local copy of the repository.
